	var fs = require('fs');
    var precess = require('child_process');


	module.exports = {

		getReqParameters : function(req)
		{
		  var params = req.body;
		  if(!params)
		  {
		    params = req.param;
		  }
		  if(!params)
		  {
		    params = req.query;
		  }
		  return params;
		},

		runCode : function(params, next)
		{    		
			var stream = fs.createWriteStream("my_file.js",{ mode: 0o775 });

			stream.once('open', function(fd) 
			{
				stream.write(params.data);
				stream.end();				  			  
			});

			stream.on('finish', function()
			{
				precess.exec("node my_file.js", 'v8.2.1', function(err , stdout, stderr)
				{
					return next(err , stdout);					
				});
			});
		},
	}