    

    module.exports = {

    	testCode : function(req , res) 
      {
          var params = codeService.getReqParameters(req);
          if(!params)
          {
            return res.json({status:300 , message:'Please Check Parameters'});
          }
          if(!params.data || !params.type)
          {
            return res.json({status:300 , message:"Please check parameters"});
          }

          if(params.type == 'node' || params.type == 'javascript')
          {
            codeService.runCode(params, function(err , result)
            {
              if(err)
              {
                  return res.json({status:300 , message:"Something went wrong "+err});    
              }
              if(params.isJoin == true)
              {
                req.socket.broadcast.to('myroom').emit('compiledData', result);
              }  
              return res.json({status:200 , message:result});
            });
          }
          else
          {
            if(params.isJoin == true)
            {
              req.socket.broadcast.to('myroom').emit('compiledData', params.data);
            } 
            return res.json({status:200 , message:params.data});
          }    		
    	},


    join : function(req, res) 
    {
        var params = codeService.getReqParameters(req);
        if(!params)
        {
          return res.json({status:300 , message:'Please Check Parameters'});
        }
	      
        req.socket.join(params.room);

	      res.json({success: true});
  	},

    leave : function(req, res) 
    {
        var params = codeService.getReqParameters(req);
        if(!params)
        {
          return res.json({status:300 , message:'Please Check Parameters'});
        }

        sails.sockets.leave(req, params.room, function(err) {
          if (err) 
            {
              return res.json({status:300 , message:'Something went wrong '+err});
            }

            res.json({success: true});
        });
    },

  	changeData: function(req , res)
    {
        var params = codeService.getReqParameters(req);
        if(!params)
        {
          return res.json({status:300 , message:'Please Check Parameters'});
        }
  		  		
    		if(params.isJoin == true)
    		{
    			req.socket.broadcast.to('myroom').emit('textData', params.data);
    		}  		
    		res.json({success: true});
  	}


	}