<style>
.CodeMirror 
{
  border: 1px solid #eee;
  height: 100%;
}
</style>

<div class="outer">
	<div class="topNav">
		<select ng-model="type">
			<option value="text">Text</option>
			<option value="node">Node</option>
			<option value="javascript">Javascript</option>
			<option value="html">HTML</option>
		</select>
		<button type="button" ng-click="runCode()">Run</button>
		<button type="button" ng-click="leaveSession()">Leave</button>
		<button type="button" ng-click="joinSession()">Join</button>
	</div>
	<div class="Container">

		<div class="leftarea">
			<textarea ng-model="myCode" id="code" rows="5" ng-change="editorChange()"></textarea>
		</div>

		<div class="rightarea">

			<div ng-if="type != 'html'" ng-bind="result"></div>

  			<div ng-if="type=='html'" ng-bind-html="resulthtml"></div>

		</div>
	</div>
</div>
