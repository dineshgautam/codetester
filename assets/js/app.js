var app = angular.module('app', ['ui.router','ui.codemirror','ngSanitize']);

app.config(function($stateProvider, $httpProvider,$urlRouterProvider) 
{	
  $stateProvider.state('/index', {
    url: '/index',
    templateUrl: 'templates/index.tpl',
    controller: 'indexCtrl',
  });
  $urlRouterProvider.otherwise( '/index' );
})
.controller('indexCtrl', function($scope) 
{
  $scope.isJoin = false;
  var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
    mode: "application/xml",
    styleActiveLine: true,
    lineNumbers: true,
    theme:'night',
    lineWrapping: true
  });

$scope.type = 'node';

  $scope.runCode = function()
  {
    if(editor.getValue().trim() == '')
    {
      alert("Please Type Code");
      return;
    }

    io.socket.post('/testcode', {data : editor.getValue() ,type:$scope.type,isJoin:$scope.isJoin}, function serverResponded (response, JWR) {
      
      if($scope.type == 'html')
      {
        $scope.resulthtml = response.message;
        $scope.$digest();        
      }
      else
      {
        $scope.result = response.message;
        $scope.$digest();
      }      
    });
  };   

  $scope.leaveSession = function()
  {
    if($scope.isJoin == false)
    {
      alert("You are not joined with any session");
      return;
    }
    io.socket.post('/leave', {room:'myroom'}, function serverResponded (body, JWR) 
    {
      $scope.isJoin = false;
      alert("You leave the session successfully");      
    });
  };

  $scope.joinSession = function()
  {
    if($scope.isJoin == true)
    {
      alert("You are allready joined with session");
      return;
    }    

   io.socket.post('/join', {room:'myroom'}, function serverResponded (body, JWR) {
      $scope.isJoin = true;
      alert("You have joined with primary session");      
    });
  };

 editor.on("keyup", function() 
 {
    io.socket.post('/changedata',{data : editor.getValue(),isJoin:$scope.isJoin}, function serverResponded (body, JWR) {      
    });
  });

 io.socket.on("textData", function(data)
 {
  editor.getDoc().setValue(data);
 });

 io.socket.on("compiledData", function(data)
 {  
    $scope.resulthtml =  $scope.result = '';
    if($scope.type == 'html'){
      $scope.resulthtml = data;  
      $scope.$digest();      
    }
    else
    {
      $scope.result = data;
      $scope.$digest();
    }    
 }); 
});